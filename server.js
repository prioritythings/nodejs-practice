#!/usr/env/bin node
const http = require('http')
const fs = require('fs')
const events = require('events')
const eventEmitter = new events.EventEmitter()
const EVT_CONNECTION = 'connection'
const zlib = require('zlib')

// connection
const listenerConnection = (req) => {
    console.log('listen the connection')
    console.log(req)
}

eventEmitter.on(EVT_CONNECTION, listenerConnection)

function addZero(x, n) {
    while (x.toString().length < n) {
        x = "0" + x;
    }
    return x;
}
const formatTime = (d) => {
    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);
    var ms = addZero(d.getMilliseconds(), 3);
    return h + ":" + m + ":" + s + ":" + ms;
}

http.createServer((req, res) => {
    // req is an http.IncomingMessage, which is a Readable Stream
    // res is an http.ServerResponse, which is a Writable Stream    
    // compress file
    fs.createReadStream('./all.http')
        .pipe(zlib.createGzip())
        .pipe(fs.createWriteStream('./tmp/all.http.gz'))

    res.writeHead(200, { 'Content-Type': 'application/json' })
    // blocking
    const blockingData = fs.readFileSync('./all.http').toString();
    // non-blocking
    let nonBlockingData = {}
    // fire an event
    eventEmitter.emit(EVT_CONNECTION, req)
    fs.readFile('./all.http', function (err, data) {
        if (err) return console.error(err);
        nonBlockingData = data.toString();
        console.log('1:', formatTime(new Date()));
    });
    console.log('2:', formatTime(new Date()));
    res.end(JSON.stringify({
        "status": 200,
        "blockingData": blockingData,
        "nonBlockingData": nonBlockingData,
        "time": '3:' + formatTime(new Date())
    }))
    console.log('4:', formatTime(new Date()));
}).listen(8012)